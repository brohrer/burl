# burl

A lightweight reinforcement learning toolbox

## Getting started

```bash
git clone https://gitlab.com/brohrer/burl.git
python3 -m pip install -e burl
```

## Try it out

```bash
python3
```
```python3
>>> import burl.test
```

## Write your own agent

You can use the [random walk agent](burl/agents/random.py) as a template.
Some things that will help it all work together:

1. The agent module (`my_agent.py`) is in `/burl/agents/`.

1. It has a class called `Agent`.

1. The `Agent` class has a name attribute.

1. The `Agent` class `__init__()` accepts an argument.
This is the world to
be learned. That world will have an `n_actions` attribute (the number of
actions it expects) and an `n_sensors` attribute (the number of sensors it
will be reporting).

1. The `Agent` class has a `step()` method that accepts two arguments,
`sensors` and `reward`. `sensors` is an array of floats and `reward` is a float.
The `step()` method returns an action, an array of floats of size `n_actions`.

1. The agent is added to `test.py`. With an import like 

```python
from burl.agents.my_agent import Agent as MyAgent
```

and then `MyAgent` added to the list of agents to be tested.


## Write your own world

You can use the [slot machines world](burl/agents/slot_machines.py)
as a template.
Some things that will help it all work together:

1. The world module (`my_world.py`) is in `/burl/worlds/`.

1. It has a class called `World`.

1. The `World` class has a name attribute.

1. The `World` class has an `n_actions` attribute, the number of
actions it expects.

1. The `World` class has an `n_sensors` attribute, the number of
sensors it reports.

1. The `World` class has a `step()` method that accepts an arguments,
`actions`, an array of floats.
The `step()` method returns a tuple of `sensors` and `reward`.
'sensors` is an array of floats of size `n_sensors`, and
`reward` is a float.

1. The world is added to `test.py`. With an import like 

```python
from burl.worlds.my_world import World as MyWorld
```

and then `MyWorld` added to the list of worlds to be tested.
