from burl.agents.ivy_canopy_pkg.canopy import Canopy
from burl.agents.ivy_canopy_pkg.ivy import Ivy
from burl.vis.reward_vis import Vis


TEST_CONSTANT = 9

class Agent:
    def __init__(
        self,
        world,
        trace_length=None,
        decay_rate=None
    ):
        try:
            n_actions = world.n_actions
        except Exception:
            print("The world needs to have n_actions defined.")

        try:
            n_sensors = world.n_sensors
        except Exception:
            print("The world needs to have n_sensors defined.")

        self.name = "ivy-canopy"
        self.model = Canopy(
            n_actions,
            n_sensors,
            trace_length=trace_length,
            decay_rate=decay_rate,
        )
        self.planner = Ivy()
        self.vis = Vis()

    def step(self, sensors, reward):
        self.reward = reward

        self.model.update_sensors(sensors, reward)
        (conditional_predictions, uncertainties, sensors_w_reward
            ) = self.model.conditional_predict()
        actions = self.planner.plan(
            conditional_predictions, uncertainties, sensors_w_reward)
        self.model.update_actions(actions)
        self.model.update_probabilities()
        # predicted_sensors = self.model.predict(actions)

        self.vis.update(self)
        return actions
