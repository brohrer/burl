import numpy as np


class Ivy:
    def __init__(self):
        self.curiosity_update_rate = 1e-2

        self.n_actions = None
        self.n_sensors = None
        self.reward_values = None
        self.curiosities = None

    def initialize(self, uncertainties):
        self.n_sensors, self.n_actions = uncertainties.shape
        all_actions = np.zeros(self.n_actions)
        self.curiosities = np.zeros((self.n_sensors, self.n_actions))

        self.reward_values = np.zeros(self.n_sensors)
        self.reward_values[0] = 1
        self.reward_values[1] = -1

    def plan(self, conditional_predictions, uncertainties, sensors):
        """
        Calculate the value of all available actions and choose
        the one with the greatest.
        """
        if self.n_actions is None:
            self.initialize(uncertainties)

        all_actions = np.zeros(self.n_actions)

        reward_value = np.sum(
            self.reward_values[np.newaxis, :] *
            conditional_predictions, axis=1)

        self.curiosities += self.curiosity_update_rate * uncertainties
        curiosity = np.max(sensors[:, np.newaxis] * self.curiosities, axis=0)

        value = reward_value + curiosity
        all_actions[np.argmax(value)] = 1

        # Reduce curiosity where it has been fulfilled.
        curiosity_fulfillment = (
            sensors[:, np.newaxis] * all_actions[np.newaxis, :])
        self.curiosities -= curiosity_fulfillment
        self.curiosities = np.maximum(0, self.curiosities)
        actions = np.copy(all_actions[1:])
        return actions
