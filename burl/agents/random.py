import numpy as np


class Agent:
    """
    Randomly choose an action at each time step.
    Mostly for testing or establishing baselines.
    """
    def __init__( self, world):
        self.name = "random walk"
        self.n_actions = world.n_actions
        self.sensors = None
        self.reward = 0

    def step(self, sensors, reward):
        """
        Choose a random action.
        """
        self.reward = reward
        self.i_action = np.random.randint(self.n_actions)
        self.action = np.zeros(self.n_actions)
        self.action[self.i_action] = 1
        return self.action.copy()
