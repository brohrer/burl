import numpy as np

from burl.agents.ivy_canopy import Agent as IvyCanopyAgent
from burl.agents.q_learning import Agent as QLearningAgent
from burl.agents.random import Agent as RandomAgent

from burl.worlds.happy_fish import World as FishWorld
from burl.worlds.slot_machines import World as SlotMachineWorld

agents = [
    IvyCanopyAgent,
    QLearningAgent,
    RandomAgent,
]

worlds = [
    SlotMachineWorld,
    FishWorld,
]

n_steps = int(1e2)

for agent_class in agents:
    for world_class in worlds:
        print()
        try:
            world = world_class()
        except RuntimeError:
            print(f"World failed to initialize")
            continue

        try:
            agent = agent_class(world)
        except RuntimeError:
            print(f"Agent failed to initialize")
            continue

        # Check that agent and world have names
        try:
            print(f"Testing {agent.name}")
        except AttributeError:
            print("Agent is missing a name attribute.")

        try:
            print(f"                          with {world.name}.")
        except AttributeError:
            print("World is missing a name attribute.")

        try:
            # Initialize the actions to "do nothing" for the first time
            # through the loop.
            actions = np.zeros(world.n_actions)
            total_reward = 0
            for i_step in range(int(n_steps)):
                sensors, reward = world.step(actions)
                actions = agent.step(sensors, reward)
                total_reward += reward
                print(f"time step {i_step} of {n_steps}", end="\r")
            print("                                             ", end="\r")
        except RuntimeError as e:
            print("Something went wrong while running" +
                f" {agent.name} with {world.name}")
            print(e)
            continue

        print(f"Success! {agent.name} with {world.name} got reward" +
            f" of {float(total_reward):.03}")
