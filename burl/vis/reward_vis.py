import numpy as np
import matplotlib.pyplot as plt
from burl.vis.array_vis import Array2DAnimation, Array3DAnimation

markersize = 20

# palette from
# https://sarahrenaeclark.com/color-palettes-ocean-life/
coral = "#fc4f00"
salmon = "#ff7062"
dark_blue = "#245c81"
medium_blue = "#338fb8"
light_blue = "#80ddef"


class Vis:
    def __init__(self):
        left_border = 1
        right_border = .8
        bottom_border = 1
        top_border = .8
        gap = .8

        history_left = left_border
        history_bottom = bottom_border
        history_height = 1.3
        history_width = 6

        performance_left = left_border
        performance_bottom = history_bottom + history_height + gap
        performance_height = history_height
        performance_width = history_width

        total_width = history_width + left_border + right_border
        total_height = (
            performance_height +
            history_height +
            top_border +
            bottom_border +
            gap
        )

        self.fig = plt.figure(
            figsize=(total_width, total_height),
            facecolor=medium_blue)

        history_ax = self.fig.add_axes((
            history_left / total_width,
            history_bottom / total_height,
            history_width / total_width,
            history_height / total_height))
        self.history_monitor = Lifetime(history_ax)

        performance_ax = self.fig.add_axes((
            performance_left / total_width,
            performance_bottom / total_height,
            performance_width / total_width,
            performance_height / total_height))
        self.performance_monitor = Monitor(performance_ax)

        plt.ion()
        plt.show()


        self.reward_pos_animation = Array2DAnimation(name="positive rewards")
        self.reward_neg_animation = Array2DAnimation(name="negative rewards")
        self.prefix_animation = Array2DAnimation(name="prefixes")
        self.curiosity_animation = Array2DAnimation(name="curiosities")
        self.conditionals_animation = Array2DAnimation(name="conditional predictions")
        self.sequence_animation = Array3DAnimation("sequences")
        self.probability_animation = Array3DAnimation("probabilities")

        self.i_step = 0
        self.update_frequency = 10

    def update(self, agent):
        self.i_step += 1
        self.performance_monitor.update(agent.reward)
        self.history_monitor.update(agent.reward)

        # self.reward_pos_animation.update(agent.model.probabilities[:, :, 0])
        # self.reward_neg_animation.update(agent.model.probabilities[:, :, 1])
        # self.conditionals_animation.update(agent.conditional_predictions)
        # self.prefix_animation.update(agent.model.prefixes)
        # self.curiosity_animation.update(agent.planner.curiosities)
        # self.sequence_animation.update(agent.model.sequences)
        # self.probability_animation.update(agent.model.probabilities)

        if self.i_step % self.update_frequency == 0:
            self.performance_monitor.draw()
            self.fig.canvas.flush_events()


class Monitor:
    def __init__(self, ax):
        self.ax = ax
        self.ax.set_facecolor(light_blue)
        self.n_pts = 1000
        self.history = list(np.zeros(self.n_pts))
        self.timeline = np.arange(self.n_pts) - self.n_pts + 1
        self.line, = self.ax.plot(
            np.array(self.history),
            color="black",
            linewidth=.5)
        self.ax.set_xlim(self.timeline[1], self.timeline[-1])
        self.ax.set_ylim(-1, 1)
        self.ax.set_ylabel("reward")
        # self.ax.set_xlabel("time steps")
        self.ax.grid()

    def update(self, new_performance):
        self.history.pop(0)
        self.history.append(new_performance)
        self.timeline += 1

    def draw(self): 
        self.ax.set_xlim(self.timeline[1], self.timeline[-1])
        self.line.set_data(self.timeline, np.array(self.history))


class Lifetime:
    def __init__(self, ax):
        self.ax = ax
        self.ax.set_facecolor(light_blue)
        self.batch_size = 1000
        self.performance_min_magnitude = .01
        self.performance_batch = []
        self.history = [0]
        self.timeline = [0]
        self.line, = self.ax.plot(
            np.array(self.timeline),
            np.array(self.history),
            color="black",
            linewidth=.5)
        self.ax.set_xlim(0, self.batch_size)
        self.ax.set_ylim(
            -self.performance_min_magnitude,
            self.performance_min_magnitude)
        self.ax.set_ylabel("reward")
        self.ax.set_xlabel("time steps")
        self.ax.grid()

    def update(self, new_performance):
        self.performance_batch.append(new_performance)
        if len(self.performance_batch) == self.batch_size:
            batch_performance = np.mean(np.array(self.performance_batch))
            self.history.append(batch_performance)
            self.performance_batch = []
            self.timeline.append(
                int(self.batch_size * (len(self.history) - 1)))
            self.ax.set_xlim(self.timeline[0], self.timeline[-1])
            self.ax.set_ylim(
                np.minimum(
                    np.min(self.history),
                    -self.performance_min_magnitude),
                np.maximum(
                    np.max(self.history),
                    self.performance_min_magnitude))
            self.line.set_data(np.array(self.timeline), np.array(self.history))
