import numpy as np

class World:
    def __init__(self):
        """
        Set up the visualization and define the sensors and reward.
        """
        self.name = "slot machines"
        self.i_step = 0

        self.n_actions = 3
        self.actions = np.zeros(self.n_actions)
        self.n_sensors = 3
        self.sensors = np.zeros(self.n_sensors)
        self.reward = 0

        # There are three slot machines.
        # These are the probabilities that each pays out when hit.
        self.hit_rates = [.2, .4, .6]

    def step(self, actions):
        """
        Take one step forward in time.
        """
        self.i_step += 1
        self.reward = 0
        try:
            i_action = np.where(actions > 0)[0][0]
            if np.random.sample() < self.hit_rates[i_action]:
                self.reward = 1
        except Exception:
            pass

        return self.sensors.copy(), self.reward
