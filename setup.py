import os
import setuptools as st

st.setup(
    name='burl',
    version='0.0',
    description='A lightweight reinforcement learning toolbox',
    # url='http://gitlab.com/brohrer/burl',
    # download_url='https://gitlab.com/brohrer/burl/tags/',
    author='Brandon Rohrer',
    author_email='brohrer@gmail.com',
    license='MIT',
    install_requires=[
        'matplotlib',
        # 'numba',
        'numpy',
    ],
    package_data={
        "": [
            # "README.md",
            # "LICENSE",
            # os.path.join("data", data_filename),
        ],
    },
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
